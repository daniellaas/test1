import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-save-posts',
  templateUrl: './save-posts.component.html',
  styleUrls: ['./save-posts.component.css']
})
export class SavePostsComponent implements OnInit {

  posts$:Observable<any[]>;
  users$:Observable<any[]>;
  userId:string;
  like:number;
  id:string;

  constructor(private PostsService:PostsService,
    public authService:AuthService) { }

  ngOnInit() {

    this.users$ = this.authService.getusers()
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.PostsService.getlist(this.userId); 
        
      }
    )
  }

  delete(id:string){
    this.PostsService.deletePost(id,this.userId);

  }
  addlike(id:string,like:number){
    this.like = like+1;
    this.PostsService.addlike(id,this.userId,this.like)
}

  }


