import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService:AuthService) { }

  email:string;

  ngOnInit() {

    this.authService.getUser().subscribe(
      user => {
        this.email = user.email }
    ) 

  }

}
