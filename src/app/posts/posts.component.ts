import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { Comm } from '../interfaces/comm';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts$: Posts[];
  comms$: Comm[];
  // anss:string[];
  title:string;
  body:string;
  ans:string;
  num:number;
  like:number;
  userId:string;
  constructor(private PostsService: PostsService,private authService:AuthService) { }

  ngOnInit() {
    this.PostsService.getPosts()
      .subscribe(posts => this.posts$ = posts);
    this.PostsService.getComm()
      .subscribe(comms => this.comms$ = comms);
    
    this.authService.getUser().subscribe(
        user => {
          this.userId = user.uid; }
      )
    
  }

  fun(id:number){
    console.log(this.userId)
this.num = id;
console.log(this.num)
    for (let index = 0; index < this.posts$.length; index++) {
        if (this.posts$[index].id == this.num) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.like = 0;
          // this.PostsService.addPost(this.userId,this.title,this.num,this.body, this.like); 
          // this.anss[this.num] = "Saved for later viewing";

        }  
      
    }
    this.ans ="Saved for later viewing"
  }
 

}

