import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import { Posts } from './interfaces/posts';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { Comm } from './interfaces/comm';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = 'https://jsonplaceholder.typicode.com/posts/';
  apiUrlComm = 'https://jsonplaceholder.typicode.com/comments/';


  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postsCollection:AngularFirestoreCollection

  constructor(private _http: HttpClient, private db:AngularFirestore) {
    
   }
   getPosts() {
    return this._http.get<Posts[]>(this.apiUrl);
  }
  getComm() {
    return this._http.get<Comm[]>(this.apiUrlComm);  
  }

  addPost(userId:string, title:string, num:number, body:string, like:number){
    const post = {title:title, num:num, body:body, like:like}
    this.userCollection.doc(userId).collection('post').add(post);
  }

  getlist(userId): Observable<any[]> {
    
    this.postsCollection = this.db.collection(`users/${userId}/post`);
    console.log('posts collection created');
    return this.postsCollection.snapshotChanges().pipe(
      map(collection => collection.map(document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }))
    );    
  } 

  
deletePost(id:string,userId:string){
  this.db.doc(`users/${userId}/post/${id}`).delete();
}

addlike(id:string, userId:string, like:number){
  this.db.doc(`users/${userId}/post/${id}`).update({
    like:like
  })
}
getpost(id:string, userId:string):Observable<any>{
  return this.db.doc(`users/${userId}/post/${id}`).get();
}
  
}
